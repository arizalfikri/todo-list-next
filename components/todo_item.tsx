import { Checkbox, HStack, Text } from "@chakra-ui/react";
import * as interfaces from "@/interfaces";

const TodoItem: React.FC<{ todo: interfaces.Todo }> = ({ todo }) => (
  <HStack
    key={todo.id}
    justify={"space-between"}
    borderBottom={"1px"}
    borderColor={"gray.200"}
    pb={1}
  >
    <Text as={todo.completed ? "del" : "p"}>{todo.title}</Text>
    <Checkbox size={"lg"} defaultChecked={todo.completed} readOnly></Checkbox>
  </HStack>
);

export default TodoItem;
