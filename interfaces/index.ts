export interface Todo {
  id: number;
  userId: number;
  title: string;
  completed: boolean;
}
export interface FormData {
  title: string;
}
export interface Props {
  initialData: Todo[];
}
