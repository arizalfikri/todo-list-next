import axios from "axios";
import * as interfaces from "@/interfaces";

const base_url = "https://jsonplaceholder.typicode.com/";

export const fetchTodos = async (start = 0): Promise<interfaces.Todo[]> => {
  const { data: todos } = await axios.get(
    base_url + `todos?_start=${start}&_limit=10`
  );
  return todos;
};
export const createTodo = async (
  data: interfaces.FormData
): Promise<interfaces.Todo> => {
  const response = await axios.post(base_url + "/todos", data);

  return response.data;
};
