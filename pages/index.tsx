import {
  Box,
  Button,
  Collapse,
  Container,
  HStack,
  Input,
  InputGroup,
  InputRightElement,
  Skeleton,
  SkeletonText,
  Stack,
  Text,
  VStack,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import React, { ChangeEvent, useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import * as interfaces from "@/interfaces";
import TodoItem from "@/components/todo_item";
import { createTodo, fetchTodos } from "@/services";

const Home: React.FC<interfaces.Props> = (props) => {
  const { initialData } = props;
  const toast = useToast();

  const [start, setStart] = useState<number>(0);
  const [formData, setFormData] = useState<interfaces.FormData>({
    title: "",
  });

  const { onToggle, isOpen } = useDisclosure();
  const { data: todos, isFetching } = useQuery(
    ["todos", start],
    () => fetchTodos(start),
    {
      initialData,
    }
  );
  const { mutateAsync, isSuccess, isError, isLoading } = useMutation({
    mutationFn: (newTodo: interfaces.FormData) => {
      return createTodo(newTodo);
    },
  });

  const handlePOST = () => {
    mutateAsync(formData);
  };
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const handleNext = () => {
    setStart((prevStart) =>
      !todos ? 0 : todos.length > 0 ? prevStart + 10 : 0
    );
  };
  const handlePrev = () => {
    setStart((prevStart) => (prevStart >= 10 ? prevStart - 10 : 0));
  };

  useEffect(() => {
    if (isSuccess) toast({ status: "success", title: "success create" });
  }, [isSuccess, isError]);
  useEffect(() => {
    if (isError) toast({ status: "error", title: "error create" });
  }, [isError]);

  return (
    <VStack w={"full"} bg={"gray.100"} justify={"center"} h={"100vh"}>
      <Container bg={"white"} maxW={"xl"} shadow={"lg"} p={0} rounded={"lg"}>
        <Box borderBottom={"1px"} borderColor={"gray.300"} p={8}>
          <HStack justify={"space-between"}>
            <Text
              textTransform={"capitalize"}
              fontWeight={"bold"}
              fontSize={"large"}
            >
              todo&nbsp;list
            </Text>
            <Button size={"sm"} colorScheme="linkedin" onClick={onToggle}>
              Add&nbsp;Todo
            </Button>
          </HStack>
          <Collapse in={isOpen}>
            <Box p={2} pt={5} w={"full"}>
              <InputGroup size="md">
                <Input
                  pr="4.6rem"
                  placeholder="Enter title"
                  onChange={handleChange}
                />
                <InputRightElement width="4.5rem" pr={1}>
                  <Button
                    size="sm"
                    colorScheme="gray"
                    onClick={handlePOST}
                    isLoading={isLoading}
                  >
                    Submit
                  </Button>
                </InputRightElement>
              </InputGroup>
            </Box>
          </Collapse>
        </Box>
        <Stack p={8} spacing={3}>
          {isFetching
            ? Array.from(Array(9)).map((e, i) => (
                <HStack key={e || i} pb={1}>
                  <SkeletonText
                    noOfLines={2}
                    spacing="2"
                    w={"full"}
                    skeletonHeight="2"
                  />
                  <Skeleton w={5} h={5} />
                </HStack>
              ))
            : todos?.map((todo: interfaces.Todo, i: number) => (
                <TodoItem key={i} todo={todo} />
              ))}

          <HStack mt={5} mx={"auto"}>
            <Button
              size={"xs"}
              onClick={handlePrev}
              leftIcon={<IoIosArrowBack size={12} />}
              isDisabled={start === 0}
            >
              Prev&nbsp;Page
            </Button>
            <Button
              size={"xs"}
              onClick={handleNext}
              rightIcon={<IoIosArrowForward size={12} />}
              isDisabled={!todos ? true : todos.length > 0 ? false : true}
            >
              Next&nbsp;Page
            </Button>
          </HStack>
        </Stack>
      </Container>
    </VStack>
  );
};

export async function getServerSideProps() {
  const initialData = await fetchTodos();

  return {
    props: {
      initialData,
    },
  };
}

export default Home;
